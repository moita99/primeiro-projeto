// Armazena os valores dos inputs da tela de Cadastro
let nomeCadastro = document.getElementById("user")
let senhaCadastro = document.getElementById("pass")
let confirmacaoSenha = document.getElementById("confirmacaoPass")

// Armazena os valores dos inputs da tela de Login
let nomeLogin = document.getElementById("userLog")
let senhaLogin = document.getElementById("senhaLog")

// Armazena o nome do usuário digitado no input de exclusão 
let nomeExcluir = document.getElementById("excluir")

// Cria vetores vazios para armazenamento temporário dos dados
let nomes = []
let senhas = []


function Cadastrar(){

    // Pega valores do LocalStorage (se tiver) e armazena
    nomes = JSON.parse(localStorage.getItem("cadastro_usuario"));
    senhas = JSON.parse(localStorage.getItem("cadastro_senha"));
  
    // verifica se os inputs não estão vazios
  if(nomeCadastro.value == '' || senhaCadastro.value == ''){

      alert("Insira todos os dados necessários!")
      return;
  }

    // verifica se os dois inputs de senha são iguais
  if(senhaCadastro.value == confirmacaoSenha.value){

    // Compara se o que veio do LocalStorage é vazio  
    if (nomes == null) {

      // Se estiver vazio, recria os vetores temporários
      nomes = []
      senhas = []

      // Adiciona os valores dos inputs no início dos vetores
      nomes.push(nomeCadastro.value)
      senhas.push(senhaCadastro.value)

      // Joga para o LocalStorage novamente
      localStorage.setItem("cadastro_usuario", JSON.stringify(nomes))
      localStorage.setItem("cadastro_senha", JSON.stringify(senhas))
    
    } else {

      for(i=0; i < nomes.length; i++){

        if(nomeCadastro.value == nomes[i]){

          alert("Este usuário já está em uso.")

          document.getElementById("user").value = ""
          document.getElementById("pass").value = ""
          document.getElementById("confirmacaoPass").value = ""

          return;
        }

      }

      // Se não estiver vazio
      // Apenas adiciona os valores dos inputs após os valores que já tem nos vetores
      nomes.push(nomeCadastro.value)
      senhas.push(senhaCadastro.value)

      // Joga para o LocalStorage novamente
      localStorage.setItem("cadastro_usuario", JSON.stringify(nomes))
      localStorage.setItem("cadastro_senha", JSON.stringify(senhas))

    }

  } else {

      alert("As senhas não combinam!")
      return;

  }

    // Mostra mensagem cadastro efetuado e carrega a página de login
    alert("Seu cadastro foi efetuado com sucesso!");
    // Pula para a página de Login
    window.location.href="loginHtml.html"

}

function Logar(){

    // Pega valores do LocalStorage (se tiver) e armazena
    nomes = JSON.parse(localStorage.getItem("cadastro_usuario"))
    senhas = JSON.parse(localStorage.getItem("cadastro_senha"))

    let logou = 0

    // Realiza um loop do tamanho dos vetores
    for(i=0; i < nomes.length; i++){

        // Se o nome e senha no input do login forem iguais ao nome e senha da vez no loop
        if(nomeLogin.value == nomes[i] && senhaLogin.value == senhas[i]){

	          // Flag "logou" ativa	
	          logou = 1
	
	      }

    }
   
    if (logou == 1){

          // Mostra mensagem de login efetuado
          alert("Login efetuado")
          window.location.href ="./formulario.html"


    }else{

          // Senão, mostra mensagem de login falhou
          alert("Login falhou!")

    }   
    
}

