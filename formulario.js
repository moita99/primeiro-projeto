let inputNome = document.getElementById("name")
let inputUltimoNome = document.getElementById("lastName")
let inputCpf = document.getElementById("cpf")
let inputTelefone = document.getElementById("telefone")
let inputBlocoAp = document.getElementById("bloco")
let inputNumMoradores = document.getElementById("moradores")
let inputEmail = document.getElementById("email")

// Variável para numerar cada morador
let idMorador = 0

// Valor do checkbox
let checkbx = document.getElementById("agreement")

// Cria vetor de objeto
let dados = {}

// Cria array
let moradores = []

function Registrar(){

    // Verifica se o checkbox está ligado
    if(checkbx.checked){

        moradores = JSON.parse(localStorage.getItem("dados_moradores"))

        if(inputNome.value == '' ||
         inputUltimoNome.value == '' ||
         inputCpf.value == '' ||
         inputTelefone.value == '' ||
         inputBlocoAp.value == '' ||
         inputNumMoradores.value == '' ||
         inputEmail.value == ''){
   
           alert("Você deve inserir todos os dados!")
           return;
        }

        if(moradores == null){

            moradores = []

            dados = {

                id: ++idMorador,
                nome: inputNome.value,
                sobrenome: inputUltimoNome.value,
                cpf: inputCpf.value,
                telefone: inputTelefone.value,
                bloco: inputBlocoAp.value,
                numMoradores: inputNumMoradores.value,
                email: inputEmail.value
            }

            moradores.push(dados)
            localStorage.setItem("dados_moradores", JSON.stringify(moradores))
            
        }
        else{

            dados = {

                id: ++idMorador,
                nome: inputNome.value, 
                sobrenome: inputUltimoNome.value,
                cpf: inputCpf.value,
                telefone: inputTelefone.value,
                bloco: inputBlocoAp.value,
                numMoradores: inputNumMoradores.value,
                email: inputEmail.value
            }

            moradores.push(dados)
            localStorage.setItem("dados_moradores", JSON.stringify(moradores))
        }

      }
      else{

        alert("Voce deve aceitar os termos e regras de uso do condomínio.")
        return;
    }

    alert("Dados registrados com sucesso!")
    LimpaInputs()
}

function Redirecionar(){

    window.location.href="updateFormulario.html"
}

let inputPesquisaId = document.getElementById("pesquisaId")

function ListarMorador(){

    moradores = JSON.parse(localStorage.getItem('dados_moradores'))

    let flagListar = 0

    pesquisaId = inputPesquisaId.value
    pesquisaId -= 1

    for(i=0; i < moradores.length; i++){

        if(pesquisaId == i){

            dados = moradores[i]
            flagListar = 1
        }

    }

    if(flagListar == 1){

        document.getElementById("name").value = dados.nome
        document.getElementById("lastName").value = dados.sobrenome
        document.getElementById("cpf").value = dados.cpf
        document.getElementById("telefone").value = dados.telefone
        document.getElementById("bloco").value = dados.bloco
        document.getElementById("moradores").value = dados.numMoradores
        document.getElementById("email").value = dados.email
    }
    else{

        alert("Morador não encontrado.")
        LimpaInputs()
    }
}

function UpdateUser(){

    moradores = JSON.parse(localStorage.getItem('dados_moradores'))

    pesquisaId = inputPesquisaId.value
    pesquisaId -= 1

    if(inputNome.value == '' ||
    inputUltimoNome.value == '' ||
    inputCpf.value == '' ||
    inputTelefone.value == '' ||
    inputBlocoAp.value == '' ||
    inputNumMoradores.value == '' ||
    inputEmail.value == ''){

      alert("Você deve inserir todos os dados!")
      return;
   }

    dados = {

        id: idMorador,
        nome: inputNome.value,
        sobrenome: inputUltimoNome.value,
        cpf: inputCpf.value,
        telefone: inputTelefone.value,
        bloco: inputBlocoAp.value,
        numMoradores: inputNumMoradores.value,
        email: inputEmail.value
    }

    moradores.splice(pesquisaId, 1, dados)
    localStorage.setItem("dados_moradores", JSON.stringify(moradores))

    alert("Dados atualizados com sucesso!")
    
    LimpaInputs()
    document.getElementById("pesquisaId").value = ""
}

function DeletarMorador(){

    moradores = JSON.parse(localStorage.getItem('dados_moradores'))
    pesquisaId = inputPesquisaId.value
    pesquisaId -= 1

    let flagExclusao = 0

    if(inputNome.value == '' ||
    inputUltimoNome.value == '' ||
    inputCpf.value == '' ||
    inputTelefone.value == '' ||
    inputBlocoAp.value == '' ||
    inputNumMoradores.value == '' ||
    inputEmail.value == ''){

      alert("Você deve pesquisar um morador primeiro!")
      return;
    }

    for(i=0; i < moradores.length; i++){

        if(pesquisaId == i){

            flagExclusao = 1
        }
    }

    if(flagExclusao == 1){

        alert("Dados de morador deletados com sucesso!")
        moradores.splice(pesquisaId, 1)
        localStorage.setItem("dados_moradores", JSON.stringify(moradores))
        LimpaInputs()
    }

}

function LimpaInputs(){

    document.getElementById("name").value = ""
    document.getElementById("lastName").value = ""
    document.getElementById("cpf").value = ""
    document.getElementById("telefone").value = ""
    document.getElementById("bloco").value = ""
    document.getElementById("moradores").value = ""
    document.getElementById("email").value = ""
}

